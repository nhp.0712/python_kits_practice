def wordsCount():
	s = '''West Lake was created from a curved part of Red River and appeared in several Vietnamese legends. One legend suggests that West Lake was shaped after the battle between Lạc Long Quân and a nine-tailed fox spirit, and that's why the lake was once called "Fox Corpse Swamp" (Đầm Xác Cáo). Another folk story claimed that original name of the lake was "Golden Buffalo Lake" (Hồ Trâu Vàng, or Han Viet: Hồ Kim Ngưu) because it was formed from struggle of a buffalo after the disappearance of her calf. In the 11th century, the lake was named "Foggy Lake" (Han Viet: Hồ Dâm Đàm) due to its misty condition.'''

	
	for c in s:
		if(not c.isalnum() and c != ' '):
			s = s.replace(c,"")
	s = s.lower()
	newS = s.split()

	print("Total words:",len(newS))

	uniqueS = set(newS)
	print("Unique words:",len(uniqueS))

	histoWord = list(newS)
	print("Frequency histogram of words")
	print("============================")

	for w in uniqueS:
		print(w, ": ", histoWord.count(w))

wordsCount()

